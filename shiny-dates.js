var styleFixes = document.createElement('style');
styleFixes.innerHTML = `
  ul#calendarlistcontainer { display: none }
  #app-settings { display: none }
  #app-navigation { float: none; height: auto; width: 100% }
  #datepickercontainer .togglebuttons + .togglebuttons { display: none }
`;
document.head.appendChild(styleFixes);
