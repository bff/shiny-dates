```
 _____ _     _              ______      _
/  ___| |   (_)             |  _  \    | |
\ `--.| |__  _ _ __  _   _  | | | |__ _| |_ ___  ___
 `--. \ '_ \| | '_ \| | | | | | | / _` | __/ _ \/ __|
/\__/ / | | | | | | | |_| | | |/ / (_| | ||  __/\__ \
\____/|_| |_|_|_| |_|\__, | |___/ \__,_|\__\___||___/
                      __/ |
                     |___/
```

> Making NextCloud calendar slightly less annoying via a Chrome extension

### Prologue

_Sometimes, when you're a teacher, there are just a lot of things you want to do with calendars that push modern calendaring software to their limits. And sometimes, there are simple things your husband can accomplish in an hour or two because he loves you and saddled you with inferior software because of silly choices he himself made in the past which he now regrets and you're too kind. This is one of the latter situations._

### What Is It?

Previously, there was a persistent app navigation bar which constricted the overall width of the screen roughly 10%. Normally this is fine, but inside the calendar on week view, each day is already a very narrow column packed with lots of events.

![Empty calendar showing the amount of space wasted on sidebar](img/before.png)

In order to get a decent overview of a week at a time, its helpful to dedicate every spare pixel to horizontal space. This plugin just hides all the calendar pickers and settings buttons, and moves the date pickers (day / week / month view and forward / backward arrows) above the calendar itself creating more horizontal space in exchange for less vertical space.

![Empty calendar showing sidebar moved to top of page](img/after.png)

## Installation

 1. Open Chrome
 2. [Download this extension](https://gitlab.com/bff/shiny-dates/-/archive/master/shiny-dates-master.zip)
 3. Unzip the files somewhere by double clicking on shiny-dates-master.zip (remember this directory_
 4. Type chrome://extensions/ into you address bar and press ENTER
 5. Clicked the button "Load unpacked" at the top left corner
 ![](img/load-unpacked-extension.png)
 6. Pick the folder itself where you unzipped this extension
 ![](img/select-folder-itself.png)
 7. [Visit your calendar](https://box.fisher-fleig.me/cloud/index.php/apps/calendar/) to make sure everything is working
